@extends('adminbase::template')
@section('all')
<div class="auth">
    <div class="auth-container">
        <div class="card">
            <header class="auth-header">
                <h1 class="auth-title">
                    <div class="logo">
                        <span class="l l1"></span>
                        <span class="l l2"></span>
                        <span class="l l3"></span>
                        <span class="l l4"></span>
                        <span class="l l5"></span>
                    </div> ModularAdmin </h1>
            </header>
            <div class="auth-content">
                <p class="text-center">RECUPERAR SENHA</p>
                <p class="text-muted text-center">
                    <small>Entre com seu email para recuperar sua senha.</small>
                </p>
                <form id="reset-form" action="" method="GET" novalidate="">
                    <div class="form-group">
                        <label for="email1">Email</label>
                        <input type="email" class="form-control underlined" name="email1" id="email1" placeholder="Seu endereço de email" required> </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary">Recuperar</button>
                    </div>
                    <div class="form-group clearfix">
                        <a class="pull-left" href="">retorne ao Login</a>
                        <a class="pull-right" href="">Registre-se!</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection