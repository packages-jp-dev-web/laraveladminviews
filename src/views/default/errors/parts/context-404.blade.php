@if(view()->exists('admin.auxAdminBase.context-404'))
@include('admin.auxAdminBase.context-404')
@else
<article class="content error-404-page">
    <section class="section">
        <div class="error-card">
            <div class="error-title-block">
                <h1 class="error-title">404</h1>
                <h2 class="error-sub-title"> Desculpe, página não encontrada </h2>
            </div>
            <div class="error-container">
               <!-- <p>You better try our awesome search:</p>
                <div class="row">
                    <div class="col-12">
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button">Search</button>
                            </span>
                        </div>
                    </div>
                </div>
                <br>-->
                <a class="btn btn-primary" href="{{url(App\Utilitys\ManageUrl::admin('home'))}}">
                    <i class="fa fa-angle-left mr-3"></i>Voltar ao Home</a>
            </div>
        </div>
    </section>
</article>
@endif