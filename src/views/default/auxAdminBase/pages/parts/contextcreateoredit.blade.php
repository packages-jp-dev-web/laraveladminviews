<article class="content forms-page">
    <div class="title-block">
        <div class="row">
            <div class="col-8">
                @if($item)
                <h3 class="title"> Editar </h3>
                @else
                <h3 class="title"> Cadastrar </h3>
                @endif                
                <p class="text-danger mt-2"> Campos obrigatórios (*). </p>
            </div>
            <div class="col-4 pl-0 text-right">
                <a href="" class="btn btn-primary back_home"><i class="fa fa-angle-double-left mr-2"></i>Voltar</a>
            </div>
        </div>                
    </div>
    <section class="section">
        <div class="row sameheight-container">
            @if(session('success'))
            <div class="col-12">
                <div class=" card card-success">
                    <div class="card-header">
                        <div class="header-block">
                            <p class="title text-white"> {{session('success')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(session('error'))
            <div class="col-12">
                <div class=" card card-danger">
                    <div class="card-header">
                        <div class="header-block">
                            <p class="title text-white"> {{session('error')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-12">
                <?php
                $url = 'create';
                if ($item) {
                    $url = 'edit';
                }
                $key = key($errors->messages());
                ?>
                @if ($key)
                @section ('js-util')
                @parent
                $('#form [name={{$key}}]') . focus();
                @stop
                @endif
                <div class="card card-block sameheight-item" style="height: 720px;">       
                    <form id="form-category" action="{{url($url)}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @if($item) 
                        <input name="slug" type="hidden" value='{{$item->slug}}'>
                        @endif
                        <div class="form-group row">
                            <div class="col-sm-6 my-2">
                                <label class="control-label">Nome * </label>
                                <input name='name' type="text" class="form-control underlined" placeholder="Dê um nome a sua categoria." @if($item) value="{{old('name',$item->name)}}" @else value="{{old('name')}}" @endif> 
                                       <small class="text-danger">{{$errors->first('name')}}</small>                                    
                            </div>                           
                        </div>
                        <div class="form-group">
                            <button dusk="save" type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>            
        </div>
    </section>    
</article>
@section('js-util')
@parent
$('.back_home').attr('href',$('#home_panel_admin').attr('href'));
@stop