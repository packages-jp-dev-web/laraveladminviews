<?php

namespace TemplateAdminJp\Tests;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UrlTest extends DuskTestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest() {
        $response = $this->get('/template-admin-views/login');
        $response->assertStatus(200);
        $response = $this->get('/template-admin-views/register');
        $response->assertStatus(200);
        $response = $this->get('/template-admin-views/reset');
        $response->assertStatus(200);
        $response = $this->get('/template-admin-views/index');
        $response->assertStatus(200);
        $response = $this->get('/template-admin-views/createoredit');
        $response->assertStatus(200);
        $response = $this->get('/template-admin-views/list');
        $response->assertStatus(200);
    }

    public function testLogin() {
        $this->browse(function (Browser $browser) {
            $browser->visit(url('/template-admin-views/login'))
                    ->assertSee('FAÇA LOGIN PARA CONTINUAR');
        });
    }
    public function testReset() {
        $this->browse(function (Browser $browser) {
            $browser->visit(url('/template-admin-views/reset'))
                    ->assertSee('RECUPERAR SENHA');
        });
    }
    public function testRegister() {
        $this->browse(function (Browser $browser) {
            $browser->visit(url('/template-admin-views/register'))
                    ->assertSee('INSCREVA-SE AGORA!!!');
        });
    }
    public function testIndex() {
        $this->browse(function (Browser $browser) {
            $browser->visit(url('/template-admin-views/index'))
                    ->assertSee('Painel Administrativo');
        });
    }
    public function testList() {
        $this->browse(function (Browser $browser) {
            $browser->visit(url('/template-admin-views/list'))
                    ->assertSee('Listar');
        });
    }
    public function testCreateOrEdit() {
        $this->browse(function (Browser $browser) {
            $browser->visit(url('/template-admin-views/createoredit'))
                    ->assertSee('Cadastrar');
        });
    }

}
