@if(view()->exists('admin.auxAdminBase.context-500-global'))
@include('admin.auxAdminBase.context-500-global')
@else
<div class="app blank sidebar-opened">
    <article class="content">
        <div class="error-card global">
            <div class="error-title-block">
                <h1 class="error-title">500</h1>
                <h2 class="error-sub-title"> Erro do Servidor Interno.</h2>
            </div>
            <div class="error-container">
                <p>Tente atualizar a sua página ou entre em contato com o suporte.</p>
                <a class="btn btn-primary" href="{{url(App\Utilitys\ManageUrl::admin('home'))}}">
                    <i class="fa fa-angle-left mr-3"></i> Voltar ao Home </a>
            </div>
        </div>
    </article>
</div>
@endif