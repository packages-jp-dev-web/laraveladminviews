<?php
$countNotifications = 0;
?>
<li class="notifications new">
    <a href="" data-toggle="dropdown">
        <i class="far fa-bell"></i>
        <sup>
            <span class="counter">{{$countNotifications}}</span>
        </sup>
    </a>
    <div class="dropdown-menu notifications-dropdown-menu">
        <ul class="notifications-container">
            @if($countNotifications < 1)
            <li>
                <a href="#" class="notification-item">
                    <div class="img-col">
                        <i class="far fa-smile-wink font-1-6em"></i>
                    </div>
                    <div class="body-col">
                        <p>
                            <span class="accent">Nenhuma nova notificação</span> </p>
                    </div>
                </a>
            </li>
            @endif
        </ul>        
    </div>
</li>