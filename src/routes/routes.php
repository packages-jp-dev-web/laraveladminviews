<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::group(['middleware' => ['web']], function () {
    Route::get('/template-admin-views/login', function () {
        return view('admin.login');
    });

    Route::get('/template-admin-views/register', function () {
        return view('admin.register');
    });

    Route::get('/template-admin-views/index', function () {
        return view('admin.index');
    });

    Route::get('/template-admin-views/reset', function () {
        return view('admin.reset-password');
    });
    Route::get('/template-admin-views/createoredit', function () {
        return view('admin.auxAdminBase.pages.createoredit', ['item' => null]);
    });
    Route::get('/template-admin-views/list', function () {
        return view('admin.auxAdminBase.pages.list', ['itens' => []]);
    });
});
