<!DOCTYPE html>
<html class="no-js" lang="pt-br">
    <head>
        @section('title')
        <title>@section('title-complement') @show</title>
        @show
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        @include('adminbase::assets.css-default')
        @section('css')
        @show
    </head>
    <body>
        @section('all')        
        <div class="main-wrapper">
            <div class="app" id="app">                
                @section('header')                
                @include('adminbase::parts.header')
                @show               
                @section('sidebar')
                @include('adminbase::parts.sidebar')
                @show
                @section('context')
                @show
                @section('footer')
                @include('adminbase::parts.footer-context')
                @show
            </div>
        </div>
        <div class="ref" id="ref">
            <div class="color-primary"></div>
            <div class="chart">
                <div class="color-primary"></div>
                <div class="color-secondary"></div>
            </div>
        </div>->
        @show
        @include('adminbase::assets.js-default')
        @section('js')
        @show
        <script type="text/javascript">
            @section('js-util')
                    @show
        </script>
    </body>
</html>
