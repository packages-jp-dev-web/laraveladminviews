<article class="content forms-page">
    <div class="title-block">
        <div class="row">
            <div class="col-8">
                <h3 class="title"> Dados do Usuário </h3>
                <p class="title-description text-danger"> Campos obrigatórios (*). </p>
            </div>
            <div class="col-4 pl-0 text-right">
                <a href="{{url(App\Utility\ManageUrl::admin('home'))}}" class="btn btn-primary"><i class="fa fa-angle-double-left mr-2"></i>Voltar</a>
            </div>
        </div>                
    </div>
    <section class="section">
        <div class="row sameheight-container">
            @if(session('success'))
            <div class="col-12">
                <div class=" card card-success">
                    <div class="card-header">
                        <div class="header-block">
                            <p class="title text-white"> {{session('success')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(session('error'))
            <div class="col-12">
                <div class=" card card-danger">
                    <div class="card-header">
                        <div class="header-block">
                            <p class="title text-white"> {{session('error')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-12">
                <div class="card card-block sameheight-item" style="height: 720px;"> 
                    <img width="100" src='{{url($user->image->way)}}'>                  
                    <form action="{{url(App\Utility\ManageUrl::admin('edit-user'))}}" method="POST" enctype="multipart/form-data" autocomplete="OFF">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <div class="col-sm-6 my-2">
                                <label class="control-label">Nome * </label>
                                <input dusk='name' name='name' type="text" class="form-control underlined" value="{{old('name',$user->name)}}"> 
                                <small class="text-danger">{{$errors->first('name')}}</small>                                    
                            </div>
                            <div class="col-sm-6 my-2">
                                <label class="control-label">Email * </label>
                                <input dusk='email' name='email' type="text" class="form-control underlined" value="{{old('email',$user->email)}}"> 
                                <small class="text-danger">{{$errors->first('email')}}</small>                                    
                            </div>
                            <div class="col-sm-6 my-2">
                                <label class="control-label">Senha </label>
                                <input dusk='password' name='password' type="password" class="form-control underlined" value="" autocomplete="off"> 
                                <small class="text-danger">{{$errors->first('password')}}</small>                                    
                            </div>
                            <div class="col-sm-6 my-2">
                                <label class="control-label">Confirmar Senha </label>
                                <input dusk='password_confirmation' name='password_confirmation' type="password" class="form-control underlined"> 
                                <small class="text-danger">{{$errors->first('password_confirmation')}}</small>                                    
                            </div>
                            <div class="col-sm-6 my-2">
                                <label class="control-label">Imagem de perfil (300kb)</label>
                                <input dusk='image' name="image" type="file" class="form-control underlined"  accept="image/*" value="{{old('image')}}">
                                <small class="text-danger">{{$errors->first('image')}}</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <button dusk="save" type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>            
        </div>
    </section>    
</article>