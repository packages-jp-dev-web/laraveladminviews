@extends('adminbase::template')
@section('title-complement')
Titulo
@stop
@section('context')
@include('admin.auxAdminBase.pages.parts.contextlist')
@include('adminbase::parts.modal-delete')
@stop

@section('css')
@parent
@include('adminbase::assets.css-datable')
@stop
@section('js')
@parent
@include('adminbase::assets.js-datable')
@stop
@section('js-util')
@parent
$(document).ready( function () {
$('#list').DataTable({
responsive: true,
"order": [[ 0, "desc" ]],
"language": {
"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
}
} );
} );
@stop