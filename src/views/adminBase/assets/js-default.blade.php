<script src="{{url('assets/adminBase/js/vendor.js')}}"></script>
<script src="{{url('assets/adminBase/js/app.js')}}"></script>
@if(view()->exists('admin.auxAdminBase.jsDefault'))
@include('admin.auxAdminBase.jsDefault')
@endif