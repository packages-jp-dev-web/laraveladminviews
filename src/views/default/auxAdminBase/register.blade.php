@extends('adminbase::template')
@section('all')
<div class="auth">
    <div class="auth-container">
        <div class="card">
            <header class="auth-header">
                <h1 class="auth-title">
                    <div class="logo">
                        <span class="l l1"></span>
                        <span class="l l2"></span>
                        <span class="l l3"></span>
                        <span class="l l4"></span>
                        <span class="l l5"></span>
                    </div> ModularAdmin </h1>
            </header>
            <div class="auth-content">
                <p class="text-center">INSCREVA-SE AGORA!!!</p>
                <form id="signup-form" action="" method="GET" novalidate="">
                    <div class="form-group">
                        <label for="firstname">Nome</label>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control underlined" name="firstname" id="firstname" placeholder="Primeiro Nome" required=""> </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control underlined" name="lastname" id="lastname" placeholder="Último Nome" required=""> </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control underlined" name="email" id="email" placeholder="Endereço de Email" required=""> </div>
                    <div class="form-group">
                        <label for="password">Senha</label>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="password" class="form-control underlined" name="password" id="password" placeholder="Senha" required=""> </div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control underlined" name="retype_password" id="retype_password" placeholder="Repita a Senha" required=""> </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="agree">
                            <input class="checkbox" name="agree" id="agree" type="checkbox" required="">
                            <span>
                                Concordo com os termos e 
                                <a href="#">política</a>
                            </span>
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary">Inscrever-se</button>
                    </div>
                    <div class="form-group">
                        <p class="text-muted text-center">
                            Já tem uma conta?
                            <a href="">Login!</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
        <div class="text-center">
            <a href="index.html" class="btn btn-secondary btn-sm">
                <i class="fa fa-arrow-left"></i> Back to dashboard </a>
        </div>
    </div>
</div>
@stop