@if(view()->exists('admin.auxAdminBase.context-500'))
@include('admin.auxAdminBase.context-500')
@else
<article class="content error-500-page">
    <section class="section">
        <div class="error-card">
            <div class="error-title-block">
                <h1 class="error-title">500</h1>
                <h2 class="error-sub-title"> Erro do Servidor Interno.</h2>
            </div>
            <div class="error-container visible">
                <p>Tente atualizar a sua página ou entre em contato com o suporte.</p>
                <a class="btn btn-primary" href="{{url(App\Utilitys\ManageUrl::admin('home'))}}">
                    <i class="fa fa-angle-left mr-3"></i> Voltar ao Home </a>
            </div>
        </div>
    </section>
</article>
@endif