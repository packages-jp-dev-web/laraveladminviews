@extends('adminBase.template')

@if(auth()->check())
@section('context')
@include('adminBase.errors.parts.context-500')
@stop
@else
@section('all')
@include('adminBase.errors.parts.context-500-global')
@stop
@endif
