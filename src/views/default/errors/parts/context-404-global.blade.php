@if(view()->exists('admin.auxAdminBase.context-404-global'))
@include('admin.auxAdminBase.context-404-global')
@else
<div class="app blank sidebar-opened">
    <article class="content">
        <div class="error-card global">
            <div class="error-title-block">
                <h1 class="error-title">404</h1>
                <h2 class="error-sub-title"> Desculpe, página não encontrada </h2>
            </div>
            <div class="error-container">
                <br>
                <a class="btn btn-primary" href="{{url(App\Utilitys\ManageUrl::admin('home'))}}">
                    <i class="fa fa-angle-left mr-3"></i>Voltar ao Home</a>
            </div>
        </div>
    </article>
</div>
@endif