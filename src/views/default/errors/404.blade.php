@extends('adminBase.template')

@if(auth()->check())
@section('context')
@include('adminBase.errors.parts.context-404')
@stop
@else
@section('all')
@include('adminBase.errors.parts.context-404-global')
@stop
@endif